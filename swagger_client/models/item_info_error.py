# coding: utf-8

"""
    Автозагрузка

    Получение отчётов о публикации и редактировании объявлений через Автозагрузку **Авито API для бизнеса предоставляется согласно [Условиям использования](https://www.avito.ru/legal/pro_tools/public-api).** <!-- ReDoc-Inject: <security-definitions> -->   # noqa: E501

    OpenAPI spec version: 1
    Contact: supportautoload@avito.ru
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class ItemInfoError(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'code': 'int',
        'description': 'str',
        'title': 'str',
        'type': 'str',
        'updated_at': 'datetime'
    }

    attribute_map = {
        'code': 'code',
        'description': 'description',
        'title': 'title',
        'type': 'type',
        'updated_at': 'updated_at'
    }

    def __init__(self, code=None, description=None, title=None, type=None, updated_at=None):  # noqa: E501
        """ItemInfoError - a model defined in Swagger"""  # noqa: E501
        self._code = None
        self._description = None
        self._title = None
        self._type = None
        self._updated_at = None
        self.discriminator = None
        self.code = code
        self.description = description
        self.title = title
        self.type = type
        self.updated_at = updated_at

    @property
    def code(self):
        """Gets the code of this ItemInfoError.  # noqa: E501

        Код ошибки или предупреждения  # noqa: E501

        :return: The code of this ItemInfoError.  # noqa: E501
        :rtype: int
        """
        return self._code

    @code.setter
    def code(self, code):
        """Sets the code of this ItemInfoError.

        Код ошибки или предупреждения  # noqa: E501

        :param code: The code of this ItemInfoError.  # noqa: E501
        :type: int
        """
        if code is None:
            raise ValueError("Invalid value for `code`, must not be `None`")  # noqa: E501

        self._code = code

    @property
    def description(self):
        """Gets the description of this ItemInfoError.  # noqa: E501

        Описание ошибки или предупреждения  # noqa: E501

        :return: The description of this ItemInfoError.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this ItemInfoError.

        Описание ошибки или предупреждения  # noqa: E501

        :param description: The description of this ItemInfoError.  # noqa: E501
        :type: str
        """
        if description is None:
            raise ValueError("Invalid value for `description`, must not be `None`")  # noqa: E501

        self._description = description

    @property
    def title(self):
        """Gets the title of this ItemInfoError.  # noqa: E501

        Название ошибки или предупреждения  # noqa: E501

        :return: The title of this ItemInfoError.  # noqa: E501
        :rtype: str
        """
        return self._title

    @title.setter
    def title(self, title):
        """Sets the title of this ItemInfoError.

        Название ошибки или предупреждения  # noqa: E501

        :param title: The title of this ItemInfoError.  # noqa: E501
        :type: str
        """
        if title is None:
            raise ValueError("Invalid value for `title`, must not be `None`")  # noqa: E501

        self._title = title

    @property
    def type(self):
        """Gets the type of this ItemInfoError.  # noqa: E501

        Тип ошибки или предупреждения   - error – Критическая ошибка, которая влияет на выгрузку: объявление не редактируется или не публикуется   - warning – Некритическая ошибка, которая в будущем может повлиять на выгрузку или влияет сейчас на необязательный параметр   - alarm – Предупреждение, которое не влияет на выгрузку   - info – Информационное сообщение   # noqa: E501

        :return: The type of this ItemInfoError.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this ItemInfoError.

        Тип ошибки или предупреждения   - error – Критическая ошибка, которая влияет на выгрузку: объявление не редактируется или не публикуется   - warning – Некритическая ошибка, которая в будущем может повлиять на выгрузку или влияет сейчас на необязательный параметр   - alarm – Предупреждение, которое не влияет на выгрузку   - info – Информационное сообщение   # noqa: E501

        :param type: The type of this ItemInfoError.  # noqa: E501
        :type: str
        """
        if type is None:
            raise ValueError("Invalid value for `type`, must not be `None`")  # noqa: E501
        allowed_values = ["error", "warning", "info", "alarm"]  # noqa: E501
        if type not in allowed_values:
            raise ValueError(
                "Invalid value for `type` ({0}), must be one of {1}"  # noqa: E501
                .format(type, allowed_values)
            )

        self._type = type

    @property
    def updated_at(self):
        """Gets the updated_at of this ItemInfoError.  # noqa: E501

        Дата актуальности ошибки или предупреждения  # noqa: E501

        :return: The updated_at of this ItemInfoError.  # noqa: E501
        :rtype: datetime
        """
        return self._updated_at

    @updated_at.setter
    def updated_at(self, updated_at):
        """Sets the updated_at of this ItemInfoError.

        Дата актуальности ошибки или предупреждения  # noqa: E501

        :param updated_at: The updated_at of this ItemInfoError.  # noqa: E501
        :type: datetime
        """
        if updated_at is None:
            raise ValueError("Invalid value for `updated_at`, must not be `None`")  # noqa: E501

        self._updated_at = updated_at

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ItemInfoError, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ItemInfoError):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
