# coding: utf-8

"""
    Автозагрузка

    Получение отчётов о публикации и редактировании объявлений через Автозагрузку **Авито API для бизнеса предоставляется согласно [Условиям использования](https://www.avito.ru/legal/pro_tools/public-api).** <!-- ReDoc-Inject: <security-definitions> -->   # noqa: E501

    OpenAPI spec version: 1
    Contact: supportautoload@avito.ru
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

import pprint
import re  # noqa: F401

import six

class ReportAutoloadMessages(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    """
    Attributes:
      swagger_types (dict): The key is attribute name
                            and the value is attribute type.
      attribute_map (dict): The key is attribute name
                            and the value is json key in definition.
    """
    swagger_types = {
        'code': 'int',
        'description': 'str',
        'element_name': 'str',
        'help_url': 'str',
        'type': 'str'
    }

    attribute_map = {
        'code': 'code',
        'description': 'description',
        'element_name': 'element_name',
        'help_url': 'help_url',
        'type': 'type'
    }

    def __init__(self, code=None, description=None, element_name=None, help_url=None, type=None):  # noqa: E501
        """ReportAutoloadMessages - a model defined in Swagger"""  # noqa: E501
        self._code = None
        self._description = None
        self._element_name = None
        self._help_url = None
        self._type = None
        self.discriminator = None
        if code is not None:
            self.code = code
        if description is not None:
            self.description = description
        if element_name is not None:
            self.element_name = element_name
        if help_url is not None:
            self.help_url = help_url
        if type is not None:
            self.type = type

    @property
    def code(self):
        """Gets the code of this ReportAutoloadMessages.  # noqa: E501

        Условный код сообщения об ошибке для однозначной идентификации сообщения  # noqa: E501

        :return: The code of this ReportAutoloadMessages.  # noqa: E501
        :rtype: int
        """
        return self._code

    @code.setter
    def code(self, code):
        """Sets the code of this ReportAutoloadMessages.

        Условный код сообщения об ошибке для однозначной идентификации сообщения  # noqa: E501

        :param code: The code of this ReportAutoloadMessages.  # noqa: E501
        :type: int
        """

        self._code = code

    @property
    def description(self):
        """Gets the description of this ReportAutoloadMessages.  # noqa: E501

        Текстовое описание проблемы и возможных способов решения  # noqa: E501

        :return: The description of this ReportAutoloadMessages.  # noqa: E501
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description):
        """Sets the description of this ReportAutoloadMessages.

        Текстовое описание проблемы и возможных способов решения  # noqa: E501

        :param description: The description of this ReportAutoloadMessages.  # noqa: E501
        :type: str
        """

        self._description = description

    @property
    def element_name(self):
        """Gets the element_name of this ReportAutoloadMessages.  # noqa: E501

        Название элемента в XML по которому выводится сообшение  # noqa: E501

        :return: The element_name of this ReportAutoloadMessages.  # noqa: E501
        :rtype: str
        """
        return self._element_name

    @element_name.setter
    def element_name(self, element_name):
        """Sets the element_name of this ReportAutoloadMessages.

        Название элемента в XML по которому выводится сообшение  # noqa: E501

        :param element_name: The element_name of this ReportAutoloadMessages.  # noqa: E501
        :type: str
        """

        self._element_name = element_name

    @property
    def help_url(self):
        """Gets the help_url of this ReportAutoloadMessages.  # noqa: E501

        URL-адрес документации по данному элементу  # noqa: E501

        :return: The help_url of this ReportAutoloadMessages.  # noqa: E501
        :rtype: str
        """
        return self._help_url

    @help_url.setter
    def help_url(self, help_url):
        """Sets the help_url of this ReportAutoloadMessages.

        URL-адрес документации по данному элементу  # noqa: E501

        :param help_url: The help_url of this ReportAutoloadMessages.  # noqa: E501
        :type: str
        """

        self._help_url = help_url

    @property
    def type(self):
        """Gets the type of this ReportAutoloadMessages.  # noqa: E501

        Тип собщения: warning, alarm, error  # noqa: E501

        :return: The type of this ReportAutoloadMessages.  # noqa: E501
        :rtype: str
        """
        return self._type

    @type.setter
    def type(self, type):
        """Sets the type of this ReportAutoloadMessages.

        Тип собщения: warning, alarm, error  # noqa: E501

        :param type: The type of this ReportAutoloadMessages.  # noqa: E501
        :type: str
        """

        self._type = type

    def to_dict(self):
        """Returns the model properties as a dict"""
        result = {}

        for attr, _ in six.iteritems(self.swagger_types):
            value = getattr(self, attr)
            if isinstance(value, list):
                result[attr] = list(map(
                    lambda x: x.to_dict() if hasattr(x, "to_dict") else x,
                    value
                ))
            elif hasattr(value, "to_dict"):
                result[attr] = value.to_dict()
            elif isinstance(value, dict):
                result[attr] = dict(map(
                    lambda item: (item[0], item[1].to_dict())
                    if hasattr(item[1], "to_dict") else item,
                    value.items()
                ))
            else:
                result[attr] = value
        if issubclass(ReportAutoloadMessages, dict):
            for key, value in self.items():
                result[key] = value

        return result

    def to_str(self):
        """Returns the string representation of the model"""
        return pprint.pformat(self.to_dict())

    def __repr__(self):
        """For `print` and `pprint`"""
        return self.to_str()

    def __eq__(self, other):
        """Returns true if both objects are equal"""
        if not isinstance(other, ReportAutoloadMessages):
            return False

        return self.__dict__ == other.__dict__

    def __ne__(self, other):
        """Returns true if both objects are not equal"""
        return not self == other
