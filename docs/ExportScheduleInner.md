# ExportScheduleInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rate** | **int** | Количество объявлений за период | 
**time_slots** | **list[int]** | Период, в который будет запущена выгрузка. Допустимые значения: целые числа от 0 до 23, где 0 - это промежуток времени 00:00-01:00, а 23 - промежуток 23:00-24:00  | 
**weekdays** | **list[int]** | День недели периода. Допустимые значения: целые числа от 0 до 6, где 0 - понедельник, 6 - воскресенье.  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

