# InlineResponse2005

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**list[ItemInfoReportAutoloadV2]**](ItemInfoReportAutoloadV2.md) | Список объявлений | 
**meta** | [**MetaReportItemsAutoloadV2**](MetaReportItemsAutoloadV2.md) |  | 
**report_id** | **int** | Идентификатор отчёта (ID) | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

