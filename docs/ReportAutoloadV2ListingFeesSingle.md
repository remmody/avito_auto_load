# ReportAutoloadV2ListingFeesSingle

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** | Количество объявлений, размещения которых были оплачены из кошелька | [optional] 
**total_amount** | **int** | Общая сумма средств, списанных из кошелька | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

