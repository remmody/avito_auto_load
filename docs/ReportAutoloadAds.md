# ReportAutoloadAds

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **str** | ID объявления в исходном XML-файле клиента | [optional] 
**avito_date_end** | **datetime** | Дата окончания публикации объявления | [optional] 
**avito_id** | **str** | ID опубликованного объявления в Авито | [optional] 
**messages** | [**ReportAutoloadMessages**](ReportAutoloadMessages.md) |  | [optional] 
**statuses** | [**ReportAutoloadStatuses**](ReportAutoloadStatuses.md) |  | [optional] 
**url** | **str** | URL-адрес объявления на сайте Авито | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

