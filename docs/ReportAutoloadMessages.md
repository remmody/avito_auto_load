# ReportAutoloadMessages

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **int** | Условный код сообщения об ошибке для однозначной идентификации сообщения | [optional] 
**description** | **str** | Текстовое описание проблемы и возможных способов решения | [optional] 
**element_name** | **str** | Название элемента в XML по которому выводится сообшение | [optional] 
**help_url** | **str** | URL-адрес документации по данному элементу | [optional] 
**type** | **str** | Тип собщения: warning, alarm, error | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

