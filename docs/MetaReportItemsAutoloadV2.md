# MetaReportItemsAutoloadV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**page** | **int** | Номер страницы | [optional] 
**pages** | **int** | Общее количество страниц | [optional] 
**per_page** | **int** | Количество объявлений на странице | [optional] 
**total** | **int** | Общее количество объявлений в отчёте | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

