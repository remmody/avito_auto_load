# InlineResponse2006Meta

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**page** | **int** | Номер страницы | [optional] 
**pages** | **int** | Общее количество страниц | [optional] 
**per_page** | **int** | Количество списаний на странице | [optional] 
**total** | **int** | Общее количество списаний в отчёте | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

