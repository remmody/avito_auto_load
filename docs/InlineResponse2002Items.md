# InlineResponse2002Items

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **str** | Идентификатор объявления из файла | 
**avito_id** | **int** | Идентификатор объявления на Авито | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

