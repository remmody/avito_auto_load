# ReportAutoloadV2SectionStatsSections1

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** | Количество объявлений в разделе второго уровня | 
**sections** | [**list[ReportAutoloadV2SectionStatsSections]**](ReportAutoloadV2SectionStatsSections.md) | Разделы третьего уровня с количеством объявлений в каждом | [optional] 
**slug** | **str** | ID раздела второго уровня | 
**title** | **str** | Название раздела второго уровня | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

