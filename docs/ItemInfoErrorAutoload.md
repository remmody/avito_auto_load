# ItemInfoErrorAutoload

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **int** | Идентификатор события | [optional] 
**description** | **str** | Описание | [optional] 
**manual** | **str** | Ссылка на документацию | [optional] 
**title** | **str** | Заголовок | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

