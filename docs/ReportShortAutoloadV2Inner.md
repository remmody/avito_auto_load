# ReportShortAutoloadV2Inner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**finished_at** | **datetime** | Дата и время закрытия отчёта (окончания выгрузки) | 
**id** | **int** | Идентификатор отчёта (ID) | 
**started_at** | **datetime** | Дата и время создания отчёта (начала выгрузки) | 
**status** | **str** | Статус отчёта - processing – Отчёт в процессе, загрузка ещё не завершена - success – Загрузка прошла без ошибок - success_warning – В части объявлений есть ошибки - error – Загрузка не удалась  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

