# ItemInfoAutoloadV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **str** | Идентификатор объявления из файла ([параметр Id](https://autoload.avito.ru/format/realty/#Id))  | 
**avito_date_end** | **datetime** | Дата окончания оплаченного периода размещения объявления | 
**avito_id** | **int** | Идентификатор объявления на Авито | 
**avito_status** | **str** | Статус объявления на Авито   - active – Объявление активно   - old – Срок размещения объявления истёек   - blocked – Объявление заблокировано   - rejected – Объявление отклонено для исправления нарушений   - archived – Объявление находится в Архиве   - removed – Объявление удалено навсегда  | 
**fee_info** | [**ItemInfoAutoloadV2FeeInfo**](ItemInfoAutoloadV2FeeInfo.md) |  | 
**messages** | [**list[ItemInfoError]**](ItemInfoError.md) | Ошибки или предупреждения по объявлению | 
**processing_time** | **datetime** | Дата и время последней обработки объявления | 
**section** | [**ItemInfoAutoloadV2Section**](ItemInfoAutoloadV2Section.md) |  | [optional] 
**url** | **str** | Ссылка на объявление на Авито | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

