# ItemInfoReportAutoloadV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **str** | Идентификатор объявления из файла ([параметр Id](https://autoload.avito.ru/format/realty/#Id))  | 
**applied_vas** | [**list[ItemInfoVas]**](ItemInfoVas.md) | Список примененных в конкретной выгрузке [услуг продвижения](https://support.avito.ru/partitions/131) | [optional] 
**avito_date_end** | **str** | Дата окончания оплаченного периода размещения объявления | 
**avito_id** | **int** | Идентификатор объявления на Авито | 
**avito_status** | **str** | Статус объявления на Авито   - active – Объявление активно   - old – Срок размещения объявления истёк   - blocked – Объявление заблокировано   - rejected – Объявление отклонено для исправления нарушений   - archived – Объявление находится в архиве   - removed – Объявление удалено навсегда  | 
**messages** | [**list[ItemInfoError]**](ItemInfoError.md) | Ошибки или предупреждения по объявлению | 
**section** | [**ItemInfoReportAutoloadV2Section**](ItemInfoReportAutoloadV2Section.md) |  | 
**url** | **str** | Ссылка на объявление на Авито | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

