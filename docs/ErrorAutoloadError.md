# ErrorAutoloadError

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **str** | Код ошибки | 
**fields** | [**dict(str, FieldError)**](FieldError.md) | Поля в которых возникла ошибка | [optional] 
**message** | **str** | Описание ошибки | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

