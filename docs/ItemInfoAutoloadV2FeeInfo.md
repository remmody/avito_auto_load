# ItemInfoAutoloadV2FeeInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **int** | Сумма средств, списанных из кошелька за это объявление (передаётся, если type &#x3D; single) | [optional] 
**package_id** | **int** | ID пакета размещений, из которого было списание за это объявление (передаётся, если type &#x3D; package) | [optional] 
**type** | **str** | Способ оплаты   - single – Оплата из кошелька   - package – Оплата из пакета размещений  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

