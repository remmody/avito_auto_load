# ReportAutoloadListingFee

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**amount** | **float** | Сумма списания за размещение из кошелька | [optional] 
**description** | **str** | Описание пакета/разового размещения (при оплате из кошелька) | 
**finish_time** | **datetime** | Время окончания пакета | [optional] 
**listing_left** | **int** | Cколько размещений в пакете осталось | [optional] 
**listing_used** | **int** | Cколько размещений из пакета использовано | [optional] 
**package_id** | **int** | id пакета | [optional] 
**package_size** | **int** | Cколько размещений в пакете предоплачено | [optional] 
**type** | **str** | Тип размещения | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

