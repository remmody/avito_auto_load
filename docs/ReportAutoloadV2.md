# ReportAutoloadV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**events** | [**list[ReportAutoloadV2Events]**](ReportAutoloadV2Events.md) | Ошибки и предупреждения, которые относятся ко всей выгрузке | 
**feed_url** | **str** | Ссылка на копию файла с объявлениями в момент начала загрузки | 
**finished_at** | **datetime** | Дата и время закрытия отчёта (окончания выгрузки) | 
**listing_fees** | [**ReportAutoloadV2ListingFees**](ReportAutoloadV2ListingFees.md) |  | [optional] 
**report_id** | **int** | Идентификатор отчёта (ID) | 
**section_stats** | [**ReportAutoloadV2SectionStats**](ReportAutoloadV2SectionStats.md) |  | 
**source** | **str** | Источник загрузки  - email – Загрузка файла по почте  - url – Загрузка файла по ссылке  - web – Загрузка файла вручную в профиле Авито  - openapi – Загрузка по API с помощью метода Загрузка файла по ссылке  | 
**started_at** | **datetime** | Дата и время создания отчёта (начала выгрузки) | 
**status** | **str** | Статус отчёта - processing – Отчёт в процессе, загрузка ещё не завершена - success – Загрузка прошла без ошибок - success_warning – В части объявлений есть ошибки - error – Загрузка не удалась  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

