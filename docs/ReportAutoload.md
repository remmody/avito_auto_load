# ReportAutoload

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ads** | [**list[ReportAutoloadAds]**](ReportAutoloadAds.md) | Список объявлений | 
**customer_name** | **str** | Название организации | 
**finished_at** | **datetime** | Время окончания выгрузки объявлений на сайт | 
**generated_at** | **datetime** | Время генерации данного отчета | 
**listing_fee** | [**list[ReportAutoloadListingFee]**](ReportAutoloadListingFee.md) | Использованные при публикации пакеты размещения | [optional] 
**started_at** | **datetime** | Время создания отчета | 
**status** | **str** | Общий статус выгрузки | 
**vas** | [**list[ReportAutoloadVas]**](ReportAutoloadVas.md) | Какие были применены услуги при данной выгрузке | [optional] 
**version** | **str** | Версия формата отчета | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

