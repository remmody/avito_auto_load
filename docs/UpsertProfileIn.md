# UpsertProfileIn

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agreement** | **bool** | Согласие с [правилами](https://support.avito.ru/articles/203867776) использования Авито Автозагрузки.  Обязательно, если профиль еще не существует.  | [optional] 
**autoload_enabled** | **bool** | Статус автозагрузки (вкл/выкл) | 
**report_email** | **str** | Почта, на которую будут приходить отчеты о выгрузках | 
**schedule** | [**ExportSchedule**](ExportSchedule.md) |  | 
**upload_url** | **str** | URL-адрес фида, для которого настроены регулярные выгрузки. Должен начинаться с http или https. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

