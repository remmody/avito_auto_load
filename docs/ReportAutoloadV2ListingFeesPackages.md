# ReportAutoloadV2ListingFeesPackages

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** | Количество объявлений, размещения которых были оплачены из этого пакета | [optional] 
**package_id** | **int** | ID пакета размещений | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

