# ReportAutoloadV2ListingFees

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**packages** | [**list[ReportAutoloadV2ListingFeesPackages]**](ReportAutoloadV2ListingFeesPackages.md) | Статистика по списаниям из пакетов размещений | 
**single** | [**ReportAutoloadV2ListingFeesSingle**](ReportAutoloadV2ListingFeesSingle.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

