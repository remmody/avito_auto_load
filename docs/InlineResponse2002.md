# InlineResponse2002

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**items** | [**list[InlineResponse2002Items]**](InlineResponse2002Items.md) | Список связанных идентификаторов (ID) объявлений из файла с идентификаторами (ID) на Авито | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

