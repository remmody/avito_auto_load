# InlineResponse2006

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fees** | [**list[ItemFeesInfoReportAutoloadV2]**](ItemFeesInfoReportAutoloadV2.md) | Список списаний за размещение объявлений | [optional] 
**meta** | [**InlineResponse2006Meta**](InlineResponse2006Meta.md) |  | 
**report_id** | **int** | Идентификатор отчёта (ID) | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

