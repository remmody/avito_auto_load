# InlineResponse2001Items

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **str** | Идентификатор объявления из файла | [optional] 
**avito_id** | **int** | Идентификатор объявления на Авито | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

