# ReportAutoloadV2SectionStatsSections

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** | Количество объявлений в разделе третьего уровня | 
**slug** | **str** | ID раздела третьего уровня | 
**title** | **str** | Название раздела третьего уровня | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

