# ItemFeesInfoReportAutoloadV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ad_id** | **str** | Идентификатор объявления из файла ([параметр Id](https://autoload.avito.ru/format/realty/#Id))  | 
**avito_id** | **int** | Идентификатор объявления на Авито | 
**fees_amount** | **int** | Сумма средств, списанных из кошелька, при разовой оплате либо количество размещений, списанных из пакета  | 
**fees_package_id** | **int** | ID пакета размещений | [optional] 
**fees_type** | **str** | Тип списания (разовая оплата или из пакета размещений) | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

