# avito_client.AutoloadApi

All URIs are relative to *https://api.avito.ru/*

| Method                                                                        | HTTP request                                        | Description                                                        |
| ----------------------------------------------------------------------------- | --------------------------------------------------- | ------------------------------------------------------------------ |
| [**create_or_update_profile**](AutoloadApi.md#create_or_update_profile)       | **POST** /autoload/v1/profile                       | Создание/редактирование настроек профиля пользователя автозагрузки |
| [**get_ad_ids_by_avito_ids**](AutoloadApi.md#get_ad_ids_by_avito_ids)         | **GET** /autoload/v2/items/ad_ids                   | ID объявлений из файла                                             |
| [**get_autoload_items_info_v2**](AutoloadApi.md#get_autoload_items_info_v2)   | **GET** /autoload/v2/reports/items                  | Объявления по ID в автозагрузке                                    |
| [**get_avito_ids_by_ad_ids**](AutoloadApi.md#get_avito_ids_by_ad_ids)         | **GET** /autoload/v2/items/avito_ids                | ID объявлений на Авито                                             |
| [**get_last_completed_report**](AutoloadApi.md#get_last_completed_report)     | **GET** /autoload/v2/reports/last_completed_report  | Статистика по последней выгрузке                                   |
| [**get_profile**](AutoloadApi.md#get_profile)                                 | **GET** /autoload/v1/profile                        | Получение профиля пользователя автозагрузки                        |
| [**get_report_by_id_v2**](AutoloadApi.md#get_report_by_id_v2)                 | **GET** /autoload/v2/reports/{report_id}            | Статистика по конкретной выгрузке                                  |
| [**get_report_items_by_id**](AutoloadApi.md#get_report_items_by_id)           | **GET** /autoload/v2/reports/{report_id}/items      | Все объявления из конкретной выгрузки                              |
| [**get_report_items_fees_by_id**](AutoloadApi.md#get_report_items_fees_by_id) | **GET** /autoload/v2/reports/{report_id}/items/fees | Списания за объявления в конкретной выгрузке                       |
| [**get_reports_v2**](AutoloadApi.md#get_reports_v2)                           | **GET** /autoload/v2/reports                        | Список отчётов автозагрузки                                        |
| [**upload**](AutoloadApi.md#upload)                                           | **POST** /autoload/v1/upload                        | Загрузка файла по ссылке                                           |

# **create_or_update_profile**

> create_or_update_profile(authorization, body=body)

Создание/редактирование настроек профиля пользователя автозагрузки

Предназначен для создания и управления профилем автозагрузки. Если профиля еще не существует - через этот метод можно его создать.

### Example

```python
from __future__ import print_function
import time
import avito_client
from avito_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = avito_client.AutoloadApi()
authorization = 'authorization_example' # str | Токен для авторизации
body = avito_client.UpsertProfileIn() # UpsertProfileIn |  (optional)

try:
    # Создание/редактирование настроек профиля пользователя автозагрузки
    api_instance.create_or_update_profile(authorization, body=body)
except ApiException as e:
    print("Exception when calling AutoloadApi->create_or_update_profile: %s\n" % e)
```

### Parameters

| Name              | Type                                      | Description           | Notes      |
| ----------------- | ----------------------------------------- | --------------------- | ---------- |
| **authorization** | **str**                                   | Токен для авторизации |
| **body**          | [**UpsertProfileIn**](UpsertProfileIn.md) |                       | [optional] |

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_ad_ids_by_avito_ids**

> InlineResponse2001 get_ad_ids_by_avito_ids(query, authorization)

ID объявлений из файла

Метод позволяет получить идентификаторы (ID) объявлений из файла автозагрузки по ID объявлений на Авито. 📝 [Напишите нам](https://docs.google.com/forms/d/e/1FAIpQLSdfTHlP6PKtOb08fxD7BaJ0VjtkaLXoRAswhF9gnKvrEyY16g/viewform?usp=sf_link), если в методе вам не хватает каких-либо данных.

### Example

```python
from __future__ import print_function
import time
import avito_client
from avito_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = avito_client.AutoloadApi()
query = 'query_example' # str | Список ID объявлений. Формат значения: строка с идентификаторами объявлений на Авито, перечисленными через «,» или «|».
authorization = 'authorization_example' # str | Токен для авторизации

try:
    # ID объявлений из файла
    api_response = api_instance.get_ad_ids_by_avito_ids(query, authorization)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AutoloadApi->get_ad_ids_by_avito_ids: %s\n" % e)
```

### Parameters

| Name              | Type    | Description                                                                                                          | Notes |
| ----------------- | ------- | -------------------------------------------------------------------------------------------------------------------- | ----- |
| **query**         | **str** | Список ID объявлений. Формат значения: строка с идентификаторами объявлений на Авито, перечисленными через «,» или « | ».    |
| **authorization** | **str** | Токен для авторизации                                                                                                |

### Return type

[**InlineResponse2001**](InlineResponse2001.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_autoload_items_info_v2**

> InlineResponse2004 get_autoload_items_info_v2(query, authorization)

Объявления по ID в автозагрузке

По запросу API отдаст данные по конкретным объявлениям. 📝 [Напишите нам](https://docs.google.com/forms/d/e/1FAIpQLSdfTHlP6PKtOb08fxD7BaJ0VjtkaLXoRAswhF9gnKvrEyY16g/viewform?usp=sf_link), если в методе вам не хватает каких-либо данных.

### Example

```python
from __future__ import print_function
import time
import avito_client
from avito_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = avito_client.AutoloadApi()
query = 'query_example' # str | Идентификаторы объявлений из файла ([параметр Id](https://autoload.avito.ru/format/realty/#Id)). Формат значения: строка, содержащая от 1 до 100 идентификаторов, перечисленных через «,» или «|».
authorization = 'authorization_example' # str | Токен для авторизации

try:
    # Объявления по ID в автозагрузке
    api_response = api_instance.get_autoload_items_info_v2(query, authorization)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AutoloadApi->get_autoload_items_info_v2: %s\n" % e)
```

### Parameters

| Name              | Type    | Description                                                                                                                                                                                     | Notes |
| ----------------- | ------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----- |
| **query**         | **str** | Идентификаторы объявлений из файла ([параметр Id](https://autoload.avito.ru/format/realty/#Id)). Формат значения: строка, содержащая от 1 до 100 идентификаторов, перечисленных через «,» или « | ».    |
| **authorization** | **str** | Токен для авторизации                                                                                                                                                                           |

### Return type

[**InlineResponse2004**](InlineResponse2004.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_avito_ids_by_ad_ids**

> InlineResponse2002 get_avito_ids_by_ad_ids(query, authorization)

ID объявлений на Авито

Метод позволяет получить идентификаторы (ID) объявлений на Авито по идентификаторам объявлений из файла автозагрузки. 📝 [Напишите нам](https://docs.google.com/forms/d/e/1FAIpQLSdfTHlP6PKtOb08fxD7BaJ0VjtkaLXoRAswhF9gnKvrEyY16g/viewform?usp=sf_link), если в методе вам не хватает каких-либо данных.

### Example

```python
from __future__ import print_function
import time
import avito_client
from avito_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = avito_client.AutoloadApi()
query = 'query_example' # str | Список ID объявлений. Формат значения: строка с [идентификаторами объявлений из файла](https://autoload.avito.ru/format/realty/#Id), перечисленными через «,» или «|»
authorization = 'authorization_example' # str | Токен для авторизации

try:
    # ID объявлений на Авито
    api_response = api_instance.get_avito_ids_by_ad_ids(query, authorization)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AutoloadApi->get_avito_ids_by_ad_ids: %s\n" % e)
```

### Parameters

| Name              | Type    | Description                                                                                                                                                         | Notes |
| ----------------- | ------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----- |
| **query**         | **str** | Список ID объявлений. Формат значения: строка с [идентификаторами объявлений из файла](https://autoload.avito.ru/format/realty/#Id), перечисленными через «,» или « | »     |
| **authorization** | **str** | Токен для авторизации                                                                                                                                               |

### Return type

[**InlineResponse2002**](InlineResponse2002.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_last_completed_report**

> ReportAutoloadV2 get_last_completed_report(authorization)

Статистика по последней выгрузке

Метод возвращает сводную статистику с результатами последней завершённой выгрузки. Например, сколько объявлений было в файле и сколько из них было опубликовано с ошибками или без. 📝 [Напишите нам](https://docs.google.com/forms/d/e/1FAIpQLSdfTHlP6PKtOb08fxD7BaJ0VjtkaLXoRAswhF9gnKvrEyY16g/viewform?usp=sf_link), если в методе вам не хватает каких-либо данных.

### Example

```python
from __future__ import print_function
import time
import avito_client
from avito_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = avito_client.AutoloadApi()
authorization = 'authorization_example' # str | Токен для авторизации

try:
    # Статистика по последней выгрузке
    api_response = api_instance.get_last_completed_report(authorization)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AutoloadApi->get_last_completed_report: %s\n" % e)
```

### Parameters

| Name              | Type    | Description           | Notes |
| ----------------- | ------- | --------------------- | ----- |
| **authorization** | **str** | Токен для авторизации |

### Return type

[**ReportAutoloadV2**](ReportAutoloadV2.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_profile**

> InlineResponse200 get_profile(authorization)

Получение профиля пользователя автозагрузки

Возвращает настройки профиля пользователя автозагрузки.

### Example

```python
from __future__ import print_function
import time
import avito_client
from avito_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = avito_client.AutoloadApi()
authorization = 'authorization_example' # str | Токен для авторизации

try:
    # Получение профиля пользователя автозагрузки
    api_response = api_instance.get_profile(authorization)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AutoloadApi->get_profile: %s\n" % e)
```

### Parameters

| Name              | Type    | Description           | Notes |
| ----------------- | ------- | --------------------- | ----- |
| **authorization** | **str** | Токен для авторизации |

### Return type

[**InlineResponse200**](InlineResponse200.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_report_by_id_v2**

> ReportAutoloadV2 get_report_by_id_v2(report_id, authorization)

Статистика по конкретной выгрузке

Метод возвращает сводную статистику с результатами конкретной выгрузки — по ID отчёта. Например, сколько объявлений было в файле и сколько из них было опубликовано с ошибками или без. 📝 [Напишите нам](https://docs.google.com/forms/d/e/1FAIpQLSdfTHlP6PKtOb08fxD7BaJ0VjtkaLXoRAswhF9gnKvrEyY16g/viewform?usp=sf_link), если в методе вам не хватает каких-либо данных.

### Example

```python
from __future__ import print_function
import time
import avito_client
from avito_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = avito_client.AutoloadApi()
report_id = 56 # int | Идентификатор отчёта (ID)
authorization = 'authorization_example' # str | Токен для авторизации

try:
    # Статистика по конкретной выгрузке
    api_response = api_instance.get_report_by_id_v2(report_id, authorization)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AutoloadApi->get_report_by_id_v2: %s\n" % e)
```

### Parameters

| Name              | Type    | Description               | Notes |
| ----------------- | ------- | ------------------------- | ----- |
| **report_id**     | **int** | Идентификатор отчёта (ID) |
| **authorization** | **str** | Токен для авторизации     |

### Return type

[**ReportAutoloadV2**](ReportAutoloadV2.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_report_items_by_id**

> InlineResponse2005 get_report_items_by_id(report_id, per_page=per_page, page=page, query=query, sections=sections)

Все объявления из конкретной выгрузки

С помощью этого метода можно получить результаты обработки каждого объявления в конкретной выгрузке. 📝 [Напишите нам](https://docs.google.com/forms/d/e/1FAIpQLSdfTHlP6PKtOb08fxD7BaJ0VjtkaLXoRAswhF9gnKvrEyY16g/viewform?usp=sf_link), если в методе вам не хватает каких-либо данных.

### Example

```python
from __future__ import print_function
import time
import avito_client
from avito_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = avito_client.AutoloadApi()
report_id = 56 # int | Идентификатор отчёта (ID)
per_page = 50 # int | Количество объявлений на странице: целое число больше 0 и меньше или равно 200  (optional) (default to 50)
page = 0 # int | Номер страницы: целое число больше или равно 0  (optional) (default to 0)
query = 'query_example' # str | Фильтр по ID объявления. Формат значения: строка с [идентификаторами объявлений из файла](https://autoload.avito.ru/format/realty/#Id) или идентификаторами объявлений на Авито, перечисленными через «,» или «|».  (optional)
sections = 'sections_example' # str | Фильтр объявлений по разделам. Формат значения: строка с идентификаторами разделов, перечисленными через «,» или «|». Получить список разделов для конкретного отчёта можно с помощью метода [Статистика по конкретной выгрузке](https://developers.avito.ru/api-catalog/autoload/documentation#operation/getReportByIdV2).  (optional)

try:
    # Все объявления из конкретной выгрузки
    api_response = api_instance.get_report_items_by_id(report_id, per_page=per_page, page=page, query=query, sections=sections)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AutoloadApi->get_report_items_by_id: %s\n" % e)
```

### Parameters

| Name          | Type    | Description                                                                                                                                                                                                     | Notes                                                                                                                                                                                                    |
| ------------- | ------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------- |
| **report_id** | **int** | Идентификатор отчёта (ID)                                                                                                                                                                                       |
| **per_page**  | **int** | Количество объявлений на странице: целое число больше 0 и меньше или равно 200                                                                                                                                  | [optional] [default to 50]                                                                                                                                                                               |
| **page**      | **int** | Номер страницы: целое число больше или равно 0                                                                                                                                                                  | [optional] [default to 0]                                                                                                                                                                                |
| **query**     | **str** | Фильтр по ID объявления. Формат значения: строка с [идентификаторами объявлений из файла](https://autoload.avito.ru/format/realty/#Id) или идентификаторами объявлений на Авито, перечисленными через «,» или « | ».                                                                                                                                                                                                       | [optional] |
| **sections**  | **str** | Фильтр объявлений по разделам. Формат значения: строка с идентификаторами разделов, перечисленными через «,» или «                                                                                              | ». Получить список разделов для конкретного отчёта можно с помощью метода [Статистика по конкретной выгрузке](https://developers.avito.ru/api-catalog/autoload/documentation#operation/getReportByIdV2). | [optional] |

### Return type

[**InlineResponse2005**](InlineResponse2005.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_report_items_fees_by_id**

> InlineResponse2006 get_report_items_fees_by_id(report_id, per_page=per_page, page=page, ad_ids=ad_ids, avito_ids=avito_ids)

Списания за объявления в конкретной выгрузке

С помощью этого метода можно получить информацию о списаниях за размещение каждого объявления в конкретной выгрузке. 📝 [Напишите нам](https://docs.google.com/forms/d/e/1FAIpQLSdfTHlP6PKtOb08fxD7BaJ0VjtkaLXoRAswhF9gnKvrEyY16g/viewform?usp=sf_link), если в методе вам не хватает каких-либо данных.

### Example

```python
from __future__ import print_function
import time
import avito_client
from avito_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = avito_client.AutoloadApi()
report_id = 56 # int | Идентификатор отчёта (ID)
per_page = 100 # int | Количество объявлений на странице: целое число больше 0 и меньше или равно 200  (optional) (default to 100)
page = 0 # int | Номер страницы: целое число больше или равно 0  (optional) (default to 0)
ad_ids = 'ad_ids_example' # str | Фильтр по ID объявления. Формат значения: строка с [идентификаторами объявлений из файла](https://autoload.avito.ru/format/realty/#Id), перечисленными через «,» или «|».  (optional)
avito_ids = 'avito_ids_example' # str | Фильтр по AvitoID объявления. Формат значения: строка с идентификаторами объявлений на Авито, перечисленными через «,» или «|».  (optional)

try:
    # Списания за объявления в конкретной выгрузке
    api_response = api_instance.get_report_items_fees_by_id(report_id, per_page=per_page, page=page, ad_ids=ad_ids, avito_ids=avito_ids)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AutoloadApi->get_report_items_fees_by_id: %s\n" % e)
```

### Parameters

| Name          | Type    | Description                                                                                                                                                            | Notes                       |
| ------------- | ------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------- | ---------- |
| **report_id** | **int** | Идентификатор отчёта (ID)                                                                                                                                              |
| **per_page**  | **int** | Количество объявлений на странице: целое число больше 0 и меньше или равно 200                                                                                         | [optional] [default to 100] |
| **page**      | **int** | Номер страницы: целое число больше или равно 0                                                                                                                         | [optional] [default to 0]   |
| **ad_ids**    | **str** | Фильтр по ID объявления. Формат значения: строка с [идентификаторами объявлений из файла](https://autoload.avito.ru/format/realty/#Id), перечисленными через «,» или « | ».                          | [optional] |
| **avito_ids** | **str** | Фильтр по AvitoID объявления. Формат значения: строка с идентификаторами объявлений на Авито, перечисленными через «,» или «                                           | ».                          | [optional] |

### Return type

[**InlineResponse2006**](InlineResponse2006.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_reports_v2**

> InlineResponse2003 get_reports_v2(authorization, per_page=per_page, page=page, date_from=date_from, date_to=date_to)

Список отчётов автозагрузки

По запросу вы получите список отчётов автозагрузки. Они будут отсортированы в порядке убывания: самый свежий отчёт — в начале списка. 📝 [Напишите нам](https://docs.google.com/forms/d/e/1FAIpQLSdfTHlP6PKtOb08fxD7BaJ0VjtkaLXoRAswhF9gnKvrEyY16g/viewform?usp=sf_link), если в методе вам не хватает каких-либо данных.

### Example

```python
from __future__ import print_function
import time
import avito_client
from avito_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = avito_client.AutoloadApi()
authorization = 'authorization_example' # str | Токен для авторизации
per_page = 50 # int | Количество отчётов на странице: целое число больше 0 и меньше или равно 200.  (optional) (default to 50)
page = 0 # int | Номер страницы: целое число больше или равно 0.  (optional) (default to 0)
date_from = '2013-10-20T19:20:30+01:00' # datetime | Фильтр по дате создания отчёта «от» (от такой-то даты). Формат значения: RFC3339  (optional)
date_to = '2013-10-20T19:20:30+01:00' # datetime | Фильтр по дате создания отчёта «до» (до такой-то даты). Формат значения: RFC3339  (optional)

try:
    # Список отчётов автозагрузки
    api_response = api_instance.get_reports_v2(authorization, per_page=per_page, page=page, date_from=date_from, date_to=date_to)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling AutoloadApi->get_reports_v2: %s\n" % e)
```

### Parameters

| Name              | Type         | Description                                                                      | Notes                      |
| ----------------- | ------------ | -------------------------------------------------------------------------------- | -------------------------- |
| **authorization** | **str**      | Токен для авторизации                                                            |
| **per_page**      | **int**      | Количество отчётов на странице: целое число больше 0 и меньше или равно 200.     | [optional] [default to 50] |
| **page**          | **int**      | Номер страницы: целое число больше или равно 0.                                  | [optional] [default to 0]  |
| **date_from**     | **datetime** | Фильтр по дате создания отчёта «от» (от такой-то даты). Формат значения: RFC3339 | [optional]                 |
| **date_to**       | **datetime** | Фильтр по дате создания отчёта «до» (до такой-то даты). Формат значения: RFC3339 | [optional]                 |

### Return type

[**InlineResponse2003**](InlineResponse2003.md)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **upload**

> upload()

Загрузка файла по ссылке

Метод запускает процесс выгрузки объявлений из файла по ссылке, которая указана [в настройках автозагрузки](https://www.avito.ru/autoload/settings) в профиле Авито. В течение часа таким способом можно запустить только одну выгрузку. **Важно**: на загрузки с помощью этого метода не распространяются лимиты на количество публикаций, которые указаны в настройках автозагрузки в профиле Авито. Все объявления из файла, которые могут быть опубликованы или активированы, будут опубликованы или активированы. 📝 [Напишите нам](https://docs.google.com/forms/d/e/1FAIpQLSdfTHlP6PKtOb08fxD7BaJ0VjtkaLXoRAswhF9gnKvrEyY16g/viewform?usp=sf_link), если в методе вам не хватает каких-либо данных.

### Example

```python
from __future__ import print_function
import time
import avito_client
from avito_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = avito_client.AutoloadApi()

try:
    # Загрузка файла по ссылке
    api_instance.upload()
except ApiException as e:
    print("Exception when calling AutoloadApi->upload: %s\n" % e)
```

### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)
