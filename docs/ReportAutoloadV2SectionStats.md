# ReportAutoloadV2SectionStats

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | **int** | Количество объявлений в разделе | 
**sections** | [**list[ReportAutoloadV2SectionStatsSections1]**](ReportAutoloadV2SectionStatsSections1.md) | Разделы второго уровня с количеством объявлений в каждом | [optional] 
**slug** | **str** | ID раздела | 
**title** | **str** | Название раздела | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

