# ReportAutoloadStatuses

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**avito** | [**ReportAutoloadStatusesAvito**](ReportAutoloadStatusesAvito.md) |  | [optional] 
**general** | [**ReportAutoloadStatusesGeneral**](ReportAutoloadStatusesGeneral.md) |  | [optional] 
**processing** | [**ReportAutoloadStatusesProcessing**](ReportAutoloadStatusesProcessing.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

