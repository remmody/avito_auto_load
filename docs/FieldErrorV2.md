# FieldErrorV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**error** | [**FieldErrorV2Error**](FieldErrorV2Error.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

