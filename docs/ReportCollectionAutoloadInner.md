# ReportCollectionAutoloadInner

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**export_success** | **bool** | Успешно ли прошла выгрузка объявления на сайте | [optional] 
**finished_at** | **datetime** | Время окончания выгрузки объявлений на сайт | 
**id** | **int** | ID отчета | 
**import_success** | **bool** | Успешно ли прошел импорт файла | [optional] 
**report_status** | **str** | Сокращенный статус обработки отчета | [optional] 
**started_at** | **datetime** | Время создания отчета | 
**stat** | **object** | Суммарная статистика по объявлению в данном отчете | [optional] 
**status** | **str** | Статус обработки отчета на русском языке | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

